﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kursa4Sitairis.Core;
using Kursa4Sitairis.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Kursa4Sitairis.Pages.Catalog
{
    public class DeleteModel : PageModel
    {
        private readonly IProductData productData;

        public Product Product { get; set; }

        public DeleteModel(IProductData productData)
        {
            this.productData = productData;
        }
        public IActionResult OnGet(int productId)
        {
            Product = productData.GetById(productId);
            if (Product == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }

        public IActionResult OnPost(int productId)
        {
            var product = productData.Delete(productId);
            productData.Commit();

            if (product == null)
            {
                return RedirectToPage("./NotFound");
            }

            TempData["Message"] = $"{product.Name} has been deleted";
            return RedirectToPage("./List");
        }
    }
}