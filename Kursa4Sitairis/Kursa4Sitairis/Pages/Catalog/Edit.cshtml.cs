﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Kursa4Sitairis.Core;
using Kursa4Sitairis.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using static System.Net.Mime.MediaTypeNames;

namespace Kursa4Sitairis.Pages.Catalog
{
    public class EditModel : PageModel
    {
        private readonly IProductData productData;
        private readonly IHtmlHelper htmlHelper;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly ProductDbContext dbContext;

        [BindProperty]
        public Product Product { get; set; }
        [BindProperty]
        public IFormFile Img { get; set; }
        public IEnumerable<SelectListItem> Cuisines { get; set; }

        public EditModel(IWebHostEnvironment env, ProductDbContext dbContext, IProductData productData, IHtmlHelper htmlHelper)
        {
            this.productData = productData;
            this.htmlHelper = htmlHelper;
            this.webHostEnvironment = env;
            this.dbContext = dbContext;
        }

        public IActionResult OnGet(int? productId)
        {
            if (productId.HasValue)
            {
                Product = productData.GetById(productId.Value);
            }
            else
            {
                Product = new Product();
            }
            if (Product == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }

        public IActionResult OnPost(Product model)
        {
            //if (!ModelState.IsValid)
            //{
            //    Debug.Print("Model is not valid");
            //    return Page();
            //}

            if(Img != null)
            {
                Product.Image = GetByteArrayFromImage(Img);
            }

            if (Product.Id > 0)
            {
                Product = productData.Update(Product);
            }
            else
            {
                Product = productData.Add(Product);
            }
            productData.Commit();
            TempData["Message"] = "Товар сохранён!";
            return RedirectToPage("./Detail", new { productId = Product.Id });
        }

        private byte[] GetByteArrayFromImage(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                file.CopyTo(target);
                return target.ToArray();
            }
        }
    }
}