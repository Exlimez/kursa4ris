﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kursa4Sitairis.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kursa4Sitairis.Controllers
{
    [Route("api/products")]
    public class ProductsController : Controller
    {
        private static List<Product> Products = new List<Product>()
        {
            new Product
            {
                Id = 1,
                Name = "Michael Jordan",
                Image = null,
                Price = 1000
            },
            new Product
            {
                Id = 2,
                Name = "Jichael Mordan",
                Image = null,
                Price = 2000
            },
            new Product
            {
                Id = 3,
                Name = "Hichael Kordan",
                Image = null,
                Price = 3000
            },
        };

        // GET: ProductsList
        [HttpGet]
        public JsonResult Get()
        {
            return Json(Products);
        }

        [HttpGet("{id}")]
        // GET: ProductsList/Details/5
        public JsonResult Get(int id)
        {
            return Json(Products.ElementAt(id));
        }

        // GET: ProductsList/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductsList/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductsList/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ProductsList/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductsList/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProductsList/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}