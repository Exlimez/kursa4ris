﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Kursa4Sitairis.Core
{
    [JsonObject, Serializable]
    public class Product
    {
        public int Id { get; set; }
        [Required, StringLength(80)]
        public string Name { get; set; }
        public byte[] Image { get; set; }
        public float Price { get; set; }
        public int Category { get; set; }
    }
}
