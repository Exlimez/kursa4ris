﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kursa4Sitairis.Core
{
    [JsonObject, Serializable]
    public class Order
    {
        public int OrderId { get; set; }
        public int ContactId { get; set; }
        public ICollection<Product> ProductList { get; set; }
    }
}
