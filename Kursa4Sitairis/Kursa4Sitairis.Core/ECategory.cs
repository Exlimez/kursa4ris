﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kursa4Sitairis.Core
{
    public enum ECategory : int
    {
        All = 0,
        MilkProducts = 1,
        Grocery = 2,
        MeatAndFish = 3,
        FruitsAndVegetables = 4,
        Drinks = 5,
        Sweets = 6
    }
}
