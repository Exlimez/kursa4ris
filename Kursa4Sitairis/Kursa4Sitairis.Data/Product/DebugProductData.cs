﻿using Kursa4Sitairis.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kursa4Sitairis.Data
{
    public class DebugProductData : IProductData
    {
        List<Product> products;

        public DebugProductData()
        {
            products = new List<Product>()
            {
                new Product { Id = 1, Name = "Scott's Pizza",  Price = 100},
                new Product { Id = 2, Name = "Gandhi's Gifts",  Price = 2100},
                new Product { Id = 3, Name = "A best mexico chicken",  Price = 1200}
            };
        }

        public Product Add(Product newProduct)
        {
            products.Add(newProduct);
            newProduct.Id = products.Max(r => r.Id) + 1;
            return newProduct;
        }

        public int Commit()
        {
            return 0;
        }

        public Product Delete(int id)
        {
            var Product = products.FirstOrDefault(r => r.Id == id);
            if (Product != null)
            {
                products.Remove(Product);
            }
            return Product;
        }

        public Product GetById(int id)
        {
            return products.SingleOrDefault(r => r.Id == id);
        }

        public IEnumerable<Product> GetProductsByName(string name = null)
        {
            return from r in products
                   where string.IsNullOrEmpty(name) || r.Name.Contains(name)
                   orderby r.Name
                   select r;
        }

        public Product Update(Product updatedProduct)
        {
            var product = products.SingleOrDefault(r => r.Id == updatedProduct.Id);
            if (product != null)
            {
                product.Name = updatedProduct.Name;
                product.Price = updatedProduct.Price;
                product.Category = updatedProduct.Category;
            }
            return product;
        }
    }
}
