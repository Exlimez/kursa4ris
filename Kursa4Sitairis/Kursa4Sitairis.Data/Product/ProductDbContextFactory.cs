﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kursa4Sitairis.Data
{
    public class ProductDbContextFactory : IDesignTimeDbContextFactory<ProductDbContext>
    {
        public ProductDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProductDbContext>();
            //optionsBuilder.UseSqlServer("Data Source=(localdb)\\ProjectsV13;Initial Catalog=OdeToFood;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            //optionsBuilder.UseSqlServer("Server=db;Initial Catalog=OdeToFood;User=sa;Password=Prosto357;");
            //optionsBuilder.UseSqlServer("Server=127.0.0.1,1433;Initial Catalog=OdeToFood;User=sa;Password=Prosto357;");
            //optionsBuilder.UseSqlServer("Server=localhost;Initial Catalog=OdeToFood;User=sa;Password=Prosto357;");

            var dbContext = new ProductDbContext(optionsBuilder.Options);
            //dbContext.Database.Migrate();
            return dbContext;
        }
    }
}
