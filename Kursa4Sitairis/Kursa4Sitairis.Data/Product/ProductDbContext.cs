﻿using Kursa4Sitairis.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kursa4Sitairis.Data
{
    public class ProductDbContext : DbContext
    {
        public DbSet<Product> newProducts { get; set; }

        public ProductDbContext(DbContextOptions<ProductDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
