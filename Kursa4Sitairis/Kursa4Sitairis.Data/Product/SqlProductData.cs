﻿using Kursa4Sitairis.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kursa4Sitairis.Data
{
    public class SqlProductData : IProductData
    {
        private ProductDbContext db;

        public SqlProductData(ProductDbContext db)
        {
            this.db = db;
        }

        public Product Add(Product newRestaurant)
        {
            db.Add(newRestaurant);
            return newRestaurant;
        }

        public int Commit()
        {
            return db.SaveChanges();
        }

        public Product Delete(int id)
        {
            var restaurant = GetById(id);
            if (restaurant != null)
            {
                db.newProducts.Remove(restaurant);
            }
            return restaurant;
        }

        public Product GetById(int id)
        {
            return db.newProducts.Find(id);
        }

        public IEnumerable<Product> GetProductsByName(string name)
        {
            var query = from r in db.newProducts
                        where r.Name.StartsWith(name) || string.IsNullOrEmpty(name)
                        orderby r.Name
                        select r;

            return query;
        }

        public Product Update(Product updatedRestaurant)
        {
            var entity = db.newProducts.Attach(updatedRestaurant);
            entity.State = EntityState.Modified;
            return updatedRestaurant;
        }
    }
}
