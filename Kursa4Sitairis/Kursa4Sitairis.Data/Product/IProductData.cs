﻿using Kursa4Sitairis.Core;
using System;
using System.Collections.Generic;

namespace Kursa4Sitairis.Data
{
    public interface IProductData
    {
        IEnumerable<Product> GetProductsByName(string name);
        Product GetById(int id);
        Product Update(Product updatedRestaurant);
        Product Add(Product newRestaurant);
        Product Delete(int id);
        int Commit();
    }
}
