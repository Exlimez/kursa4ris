﻿using Kursa4Sitairis.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kursa4Sitairis.Data
{
    public class DebugContactInfoData : IContactInfoData
    {
        List<ContactInfo> contacts;

        public DebugContactInfoData()
        {
            contacts = new List<ContactInfo>()
            {
                ///new ContactInfo { Id = 1, Name = "Scott's Pizza",  Price = 100},
                ///new ContactInfo { Id = 2, Name = "Gandhi's Gifts",  Price = 2100},
                ///new ContactInfo { Id = 3, Name = "A best mexico chicken",  Price = 1200}
            };
        }

        public ContactInfo Add(ContactInfo newContactInfo)
        {
            contacts.Add(newContactInfo);
            newContactInfo.ContactId = contacts.Max(r => r.ContactId) + 1;
            return newContactInfo;
        }

        public int Commit()
        {
            return 0;
        }

        public ContactInfo Delete(int contactId)
        {
            var ContactInfo = contacts.FirstOrDefault(r => r.ContactId == contactId);
            if (ContactInfo != null)
            {
                contacts.Remove(ContactInfo);
            }
            return ContactInfo;
        }

        public ContactInfo GetById(int contactId)
        {
            return contacts.SingleOrDefault(r => r.ContactId == contactId);
        }

        public IEnumerable<ContactInfo> GetContactInfoByName(string name = null)
        {
            return from r in contacts
                   where string.IsNullOrEmpty(name) || r.ContactName.Contains(name)
                   orderby r.ContactName
                   select r;
        }

        public ContactInfo Update(ContactInfo updatedContactInfo)
        {
            var contactInfo = contacts.SingleOrDefault(r => r.ContactId == updatedContactInfo.ContactId);
            if (contactInfo != null)
            {
                contactInfo.ContactName = updatedContactInfo.ContactName;
                contactInfo.Address = updatedContactInfo.Address;
                contactInfo.City = updatedContactInfo.City;
                contactInfo.MobileNumber = updatedContactInfo.MobileNumber;
            }
            return contactInfo;
        }
    }
}
