﻿using Kursa4Sitairis.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kursa4Sitairis.Data
{
    interface IContactInfoData
    {
        IEnumerable<ContactInfo> GetContactInfoByName(string name);
        ContactInfo GetById(int id);
        ContactInfo Update(ContactInfo updatedRestaurant);
        ContactInfo Add(ContactInfo newRestaurant);
        ContactInfo Delete(int id);
        int Commit();
    }
}
