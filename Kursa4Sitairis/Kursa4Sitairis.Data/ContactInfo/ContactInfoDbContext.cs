﻿using Kursa4Sitairis.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kursa4Sitairis.Data
{
    public class ContactInfoDbContext : DbContext
    {
        public DbSet<ContactInfo> Contacts { get; set; }

        public ContactInfoDbContext(DbContextOptions<ContactInfoDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
