﻿using Kursa4Sitairis.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Kursa4Sitairis.Data
{
    public class SqlContactInfoData : IContactInfoData
    {
        private ContactInfoDbContext db;

        public SqlContactInfoData(ContactInfoDbContext db)
        {
            this.db = db;
        }

        public ContactInfo Add(ContactInfo newRestaurant)
        {
            db.Add(newRestaurant);
            return newRestaurant;
        }

        public int Commit()
        {
            return db.SaveChanges();
        }

        public ContactInfo Delete(int id)
        {
            var restaurant = GetById(id);
            if (restaurant != null)
            {
                db.Contacts.Remove(restaurant);
            }
            return restaurant;
        }

        public ContactInfo GetById(int id)
        {
            return db.Contacts.Find(id);
        }

        public IEnumerable<ContactInfo> GetContactInfoByName(string name)
        {
            var query = from r in db.Contacts
                        where r.ContactName.StartsWith(name) || string.IsNullOrEmpty(name)
                        orderby r.ContactName
                        select r;

            return query;
        }

        public ContactInfo Update(ContactInfo updatedRestaurant)
        {
            var entity = db.Contacts.Attach(updatedRestaurant);
            entity.State = EntityState.Modified;
            return updatedRestaurant;
        }
    }
}
