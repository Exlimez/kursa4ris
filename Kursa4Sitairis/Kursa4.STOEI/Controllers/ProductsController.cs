﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kursa4Sitairis.Core;
using Kursa4Sitairis.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Kursa4.STOEI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : Controller
    {
        private IProductData productData;
        
        private static List<Product> Products = new List<Product>()
        {
            new Product
            {
                Id = 1,
                Name = "Michael Jordan",
                Image = null,
                Price = 1000
            },
            new Product
            {
                Id = 2,
                Name = "Jichael Mordan",
                Image = null,
                Price = 2000
            },
            new Product
            {
                Id = 3,
                Name = "Hichael Kordan",
                Image = null,
                Price = 3000
            },
        };


        // GET: ProductsList
        [HttpGet]
        public JsonResult Get()
        {
            List<Product> products = productData.GetProductsByName("").ToList();
            List<ProductOverNetwork> productsOverNetwork = new List<ProductOverNetwork>();

            foreach (var product in products)
            {
                ProductOverNetwork productOverNetwork = new ProductOverNetwork();
                productOverNetwork.Id = product.Id;
                productOverNetwork.Name = product.Name;
                productOverNetwork.Price = product.Price;
                productOverNetwork.ImageBase64 = Convert.ToBase64String(product.Image);
                productsOverNetwork.Add(productOverNetwork);
            }
            return Json(productsOverNetwork);
            //return JsonConvert.SerializeObject(products);
        }

        [HttpGet("{id}")]
        // GET: ProductsList/Details/5
        public JsonResult Get(int id)
        {
            Product aProduct = productData.GetProductsByName("").ToList().ElementAt(id);

            ProductOverNetwork productOverNetwork = new ProductOverNetwork();
            productOverNetwork.Id = aProduct.Id; 
            productOverNetwork.Name = aProduct.Name;
            productOverNetwork.Price = aProduct.Price;
            productOverNetwork.ImageBase64 = Convert.ToBase64String(aProduct.Image);

            return Json(productOverNetwork);
            //return Json(productData.GetProductsByName("").ToList().ElementAt(id));
        }

        private readonly ILogger<ProductsController> _logger;

        public ProductsController(ILogger<ProductsController> logger, IProductData productData)
        {
            _logger = logger;
            this.productData = productData;
            List<Product> products = productData.GetProductsByName("").ToList();
            foreach (var product in products)
            {
                _logger.LogInformation(product.Name);
            }
        }

    }
}
