﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ProductsList", order = 1)]
public class ProductsList : ScriptableObject
{
    public List<ProductOverNetwork> productsInOrderList = new List<ProductOverNetwork>(); 
}
