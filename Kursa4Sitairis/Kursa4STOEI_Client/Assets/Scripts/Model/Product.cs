﻿using Newtonsoft.Json;
using System;


[JsonObject, Serializable]
public class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public byte[] Image { get; set; }
    public float Price { get; set; }
    public int Category { get; set; }

}