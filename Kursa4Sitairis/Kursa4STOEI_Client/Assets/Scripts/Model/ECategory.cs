﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public enum ECategory : int
{
    All = 0,
    MilkProducts = 1,
    Grocery = 2,
    MeatAndFish = 3,
    FruitsAndVegetables = 4,
    Drinks = 5,
    Sweets = 6
}

public static class ECategoryRus
{
    public static string ToRus(ECategory category)
    {
        switch (category)
        {
            case ECategory.All:
                return "Все товары";
            case ECategory.MilkProducts:
                return "Молочные продукты";
            case ECategory.Grocery:
                return "Бакалея";
            case ECategory.MeatAndFish:
                return "Мясо и рыба";
            case ECategory.FruitsAndVegetables:
                return "Фрукты и овощи";
            case ECategory.Drinks:
                return "Напитки";
            case ECategory.Sweets:
                return "Сладости";
            default:
                return "Error";
        }
    }
}
