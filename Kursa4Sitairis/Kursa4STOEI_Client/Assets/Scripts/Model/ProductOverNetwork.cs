﻿using Newtonsoft.Json;
using System;


[JsonObject, Serializable]
public class ProductOverNetwork
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string ImageBase64 { get; set; }
    public float Price { get; set; }
    public int Category { get; set; }

    public ProductOverNetwork()
    {

    }

    public ProductOverNetwork(Product product)
    {
        Id = product.Id;
        Name = product.Name;
        Price = product.Price;
        Category = product.Category;
        ImageBase64 = Convert.ToBase64String(product.Image);
    }
}
