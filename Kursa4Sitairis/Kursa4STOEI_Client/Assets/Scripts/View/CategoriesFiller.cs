﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CategoriesFiller : MonoBehaviour
{
    [SerializeField] private ProductsList allProductsList;
    [SerializeField] private GameObject categoryButtonPrefab;
    [SerializeField] private Transform categoriesListContentTransform;

    private GameObject categoriesMenu;

    private List<GameObject> buttonsGOList = new List<GameObject>();

    private void Start()
    {
        categoriesMenu = GameObject.FindGameObjectWithTag("CategoriesMenu");
    }

    public void OnEnable()
    {
        Array values = Enum.GetValues(typeof(ECategory));

        foreach (var value in values)
        {
            var categoryBtnGO = Instantiate(categoryButtonPrefab, categoriesListContentTransform);
            categoryBtnGO.GetComponentInChildren<TextMeshProUGUI>().text = ECategoryRus.ToRus((ECategory) value);
            categoryBtnGO.GetComponentInChildren<Category>().categoryItself = (ECategory) value;
            buttonsGOList.Add(categoryBtnGO);
        }
    }

    private void OnDisable()
    {
        foreach (var buttonGO in buttonsGOList)
        {
            Destroy(buttonGO);
        }
        categoriesMenu.SetActive(false);
    }

    public void OnClick_CategoryButton()
    {

    }
}
