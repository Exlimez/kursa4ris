﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyProduct : MonoBehaviour
{
    [SerializeField] ProductsList shoppingCart;
    [SerializeField] ProductItem productItem;

    public void OnClick_Buy()
    {
        shoppingCart.productsInOrderList.Add(productItem.ProductItself);
    }
}
