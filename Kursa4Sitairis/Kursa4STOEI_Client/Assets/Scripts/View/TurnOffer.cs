﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffer : MonoBehaviour
{
    [SerializeField] private GameObject gameObjectToSwitch;
    public void SwitchActive()
    {
        gameObjectToSwitch.SetActive(!gameObjectToSwitch.activeInHierarchy);
    }
}
