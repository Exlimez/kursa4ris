﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShoppingCartStarter : MonoBehaviour
{
    public List<GameObject> productsGOInCart = new List<GameObject>();

    [SerializeField] private ProductsList shoppingCartCache;
    [SerializeField] private GameObject shoppingCartItemPrefab;
    [SerializeField] private Transform shoppingCartContentTransform;

    

    public void OnEnable()
    {
        foreach(var product in shoppingCartCache.productsInOrderList)
        {
            var productGO = Instantiate(shoppingCartItemPrefab, shoppingCartContentTransform);
            byte[] imageBytes = Convert.FromBase64String(product.ImageBase64);
            var tex = new Texture2D(200, 200);
            tex.LoadImage(imageBytes);
            Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            productGO.GetComponentInChildren<Image>().sprite = sprite;
            productGO.GetComponentInChildren<TextMeshProUGUI>().text = product.Name;
            productGO.GetComponent<ProductItem>().ProductItself = product;
            productGO.GetComponentInChildren<DeleteProduct>().shoppingCartStarter = this;
            productsGOInCart.Add(productGO);
        }
    }

    private void OnDisable()
    {
        foreach(var productGO in productsGOInCart)
        {
            Destroy(productGO);
        }
    }
}
