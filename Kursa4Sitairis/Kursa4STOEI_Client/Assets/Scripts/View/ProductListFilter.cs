﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductListFilter : MonoBehaviour
{
    [HideInInspector] public List<GameObject> productsGOList;

    public void FilterProducts(ECategory filterCategory)
    {
        if(filterCategory == ECategory.All)
        {
            foreach (var productGO in productsGOList)
            {
                productGO.SetActive(true);
            }
        }
        else
        {
            foreach (var productGO in productsGOList)
            {
                productGO.SetActive(true);
                if (productGO.GetComponent<ProductItem>().ProductItself.Category != (int)filterCategory)
                {
                    productGO.SetActive(false);
                }
            }
        }
    }
}
