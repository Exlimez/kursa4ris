﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteProduct : MonoBehaviour
{
    [HideInInspector] public ShoppingCartStarter shoppingCartStarter;

    [SerializeField] ProductsList shoppingCart;
    [SerializeField] ProductItem productItem;

    public void OnClick_Delete()
    {
        Destroy(shoppingCartStarter.productsGOInCart.Find((x) => x.GetComponent<ProductItem>().ProductItself == productItem.ProductItself));
        shoppingCart.productsInOrderList.Remove(productItem.ProductItself);
    }
}
