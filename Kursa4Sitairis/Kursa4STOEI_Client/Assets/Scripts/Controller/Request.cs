﻿
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Request : MonoBehaviour
{
    [SerializeField] private ProductsList productsList;
    [SerializeField] private GameObject imagePrefab;
    [SerializeField] private GameObject parentContentField;
    [SerializeField] private ProductListFilter productListFilter; 

    void Start()
    {
        StartCoroutine(GetRequest("https://localhost:44338/products"));
    }

    IEnumerator GetRequest(string uri)
    {

        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();


            string[] pages = uri.Split('/');
            int page = pages.Length - 1;
            List<ProductOverNetwork> productList = JsonConvert.DeserializeObject<List<ProductOverNetwork>>(webRequest.downloadHandler.text);


            foreach(var product in productList)
            {
                var imageObject = Instantiate(imagePrefab, parentContentField.transform);
                
                imageObject.GetComponentInChildren<TextMeshProUGUI>().text = product.Name;
                imageObject.GetComponent<ProductItem>().ProductItself = product;
                if (product.ImageBase64 != null)
                {
                    byte[] imageBytes = Convert.FromBase64String(product.ImageBase64);
                    var tex = new Texture2D(200, 200);
                    tex.LoadImage(imageBytes);
                    Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                    imageObject.GetComponentInChildren<Image>().sprite = sprite;

                }
                //byte[] imageBytes = Convert.FromBase64String(product.ImageBase64);
                //var tex = new Texture2D(200, 200);
                //tex.LoadImage(imageBytes);
                //Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                

                productsList.productsInOrderList.Add(product);
                productListFilter.productsGOList.Add(imageObject);
            }
        }
    }
}
