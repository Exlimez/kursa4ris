﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryFilter : MonoBehaviour
{
    [HideInInspector] public Category category;

    public void OnClick_CategoryButton()
    {
        GameObject.FindGameObjectWithTag("CategoriesMenu").SetActive(false);
        GameObject.FindGameObjectWithTag("ProductsListMenu").GetComponent<ProductListFilter>().FilterProducts(category.categoryItself);
    }
}
